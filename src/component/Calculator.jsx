import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Button, Form, ListGroup } from "react-bootstrap";
import History from "./History";

export default class Calculator extends Component {
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
    this.validate = this.validate.bind(this);
    this.state = {
      arr: [],
    };
  }
  validate(s) {
    let patt = /[^0-9]/gi;
    if (s.match(patt) != null) {
    } else if (s === "") {
    } else {
      return false;
    }
  }

  onSubmit(e) {
    e.preventDefault();
    var a = this.a.value;
    var b = this.b.value;
    var opt = this.val.value;
    var result;

    if (this.validate(a) === false && this.validate(b) === false) {
      if (opt === "plus") {
        result = Number(a) + Number(b);
      } else if (opt === "sub") {
        result = a - b;
      } else if (opt === "mul") {
        result = a * b;
      } else if (opt === "div") {
        result = a / b;
      } else if (opt === "mol") {
        result = a % b;
      }

      this.setState({
        arr: this.state.arr.concat(result),
      });
    } else {
      alert("Cannot Calculate Number");
    }
  }

  render() {
    let myMargin = {
      marginTop: "20px",
      marginBottom: "20px",
    };

    let myBtn = {
      float: "left",
      marginTop: "20px",
    };

    let item = this.state.arr.map((a, index) => (
      <History rrr={a} key={index} />
    ));

    return (
      <div className="container">
        <div className="row">
          <div>
            <Card style={{ width: "18rem", margin: "20px" }}>
              <Card.Img
                variant="top"
                src="https://images-na.ssl-images-amazon.com/images/I/51xa4WtV1wL.png"
              />
              <Card.Body>
                <Form.Control
                  type="text"
                  ref={(aa) => (this.a = aa)}
                  name="a"
                />
                <Form.Control
                  type="text"
                  ref={(bb) => (this.b = bb)}
                  name="b"
                  style={myMargin}
                />

                <Form.Control
                  as="select"
                  ref={(dropd) => (this.val = dropd)}
                  name="val"
                >
                  <option value="plus"> + Plus</option>
                  <option value="sub"> - Subtract</option>
                  <option value="mul"> * Multiply</option>
                  <option value="div"> / Divide</option>
                  <option value="mol"> % Module</option>
                </Form.Control>

                <Button variant="primary" style={myBtn} onClick={this.onSubmit}>
                  Calculate
                </Button>
              </Card.Body>
            </Card>
          </div>

          <div>
            <h3 style={myMargin}>Result History</h3>
            <ListGroup>{item}</ListGroup>
          </div>
        </div>
      </div>
    );
  }
}
