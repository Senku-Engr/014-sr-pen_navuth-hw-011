import React from "react";
// import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
// import { Card, Button, Form } from "react-bootstrap";
import Calculator from "./component/Calculator";
// import History from "./component/History";

function App() {
  return (
    <div className="App">
      <Calculator />
    </div>
  );
}

export default App;
